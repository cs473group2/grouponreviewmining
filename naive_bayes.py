import json
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from sklearn import linear_model,cross_validation,metrics,svm,naive_bayes
import numpy as np
from sklearn.externals import joblib
import os
import sys


clfName = "Naive Bayes"
modelFolderName = "NBSavedModel"
aspectarr = ["_ambience", "_food", "_groupon", "_location", "_price", "_service"]
datafile="allreviewed.json"
dictfile="small-words-dict.txt"

# Get dictionary of words from infile to outfile.
def preprocess(infile,outfile,stopwordFile,cutsize):
  # Get lines of json objects
  rlines=open(infile,'r').read().split('\n') # split whole file into a list of lines
  lines=[line for line in rlines if len(line)>0] # remove empty lines
  linenum=len(lines)
  # Get stopword list
  stopwordList=open(stopwordFile,'r').read().split()
  wordsDict={} # words dictionary
  wnl = WordNetLemmatizer()
  tokenizer = RegexpTokenizer(r'\w+') # Only keeps chars in the set [a-zA-Z0-9_] and removes punctuations
  for i in range(linenum):
    try:
      infodict=json.loads(lines[i])
      sent=infodict["sentence"]
      rwords=tokenizer.tokenize(sent)
      words=[wnl.lemmatize(x).lower() for x in rwords]
      for word in words:
        if word not in stopwordList and len(word)>1:
          if word in wordsDict:
            wordsDict[word]["count"]+=1
          else:
            wordsDict[word]={"count":1}
    except:
      print i,lines[i]
  rwordsList=sorted(wordsDict.keys())
  index=0
  finalWordDict={}
  for word in rwordsList:
    if wordsDict[word]["count"]>cutsize:
      finalWordDict[word]={}
      finalWordDict[word]["count"]=wordsDict[word]["count"]
      finalWordDict[word]["index"]=index
      index+=1
  outf=open(outfile,"w")
  outf.write(json.dumps(finalWordDict,sort_keys=True)+"\n")
  outf.close()

def parse():
  line1=open(dictfile,'r').read().split("\n")[0]
  wordsDict=json.loads(line1)
  data=[]
  labels={}
  aslen=len(aspectarr)
  
  #obtain data and labels
  wnl = WordNetLemmatizer()
  tokenizer = RegexpTokenizer(r'\w+') # Only keeps chars in the set [a-zA-Z0-9_] and removes punctuations
  rlines=open(datafile,'r').read().split('\n')
  lines=[x for x in rlines if len(x)>0] # filter empty strings
  snum=len(lines)
  wordCount=len(wordsDict.keys())

  for i in range(aslen):
    labels[aspectarr[i]]=[]
    #labels[aspectarr[i]]=[0]*wordCount

  for i in range(snum):
    try:
      infodict=json.loads(lines[i])
    except:
      print "err:",lines[i]
      continue
    sent=infodict["sentence"]
    rwords=tokenizer.tokenize(sent)
    words=[wnl.lemmatize(x).lower() for x in rwords]
    feature=[0]*wordCount
    allwords=wordsDict.keys()
    for word in words:
      if word in allwords:
        j=wordsDict[word]["index"]
        feature[j]=wordsDict[word]["count"]
    data.append(feature)
    
    for j in range(aslen):
      if infodict[aspectarr[j]]!=0:
        labels[aspectarr[j]].append(1)
      else:
        labels[aspectarr[j]].append(0)
  for i in range(aslen):
    #try:
      #clf = naive_bayes.GaussianNB()
      clf = naive_bayes.MultinomialNB()
      clf.fit(data, labels[aspectarr[i]])
      saveModel(clf,aspectarr[i])
    #except:
    #  print len(data),len(labels[aspectarr[j]])

    

def saveModel(clf, aspect):
  fname = modelFolderName + "/" + clfName + aspect
  joblib.dump(clf, fname)
  print("Save model to %s" % fname)
  return 0

def loadModel(aspect):
  mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
  fname = mdir + modelFolderName + "/" + clfName + aspect
  clf = joblib.load(fname)
  return clf

def get_feature(sent):
  mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
  dictf=mdir +dictfile
  line1=open(dictf,'r').read().split("\n")[0]
  wordsDict=json.loads(line1)
  allwords=wordsDict.keys()
  wordCount=len(allwords)
  
  tokenizer = RegexpTokenizer(r'\w+')
  wnl = WordNetLemmatizer()
  
  rwords=tokenizer.tokenize(sent)
  words=[wnl.lemmatize(x).lower() for x in rwords]
  feature=[0]*wordCount
  
  for word in words:
    if word in allwords:
      j=wordsDict[word]["index"]
      feature[j]=wordsDict[word]["count"]
  return feature

def predict(rtext):
  ret=""
  aslen=len(aspectarr)
  feature=get_feature(rtext)
  for i in range(aslen):
    clf=loadModel(aspectarr[i])
    x=clf.predict(feature)
    ret+=str(x[0])+" "
  return ret


def train():
  preprocess(datafile,dictfile,'stopwordList.txt',0)
  parse()


if __name__ == "__main__":
  train()