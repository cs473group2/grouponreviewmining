import json
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from sklearn import linear_model,cross_validation,metrics,svm,naive_bayes
import numpy as np
from sklearn.externals import joblib
import os
import sys
import logistic_regression

clfname = "Polarity Prediction"
modelFolderName = "PolarityPredictionSavedModel"
aspectarr = ["_ambience", "_food", "_groupon", "_location", "_price", "_service"]
datafile="allreviewed.json"
dictfile="small-words-dict.txt"
polarityfile="polarity-1-1000.json"

def saveModel(clf, aspect):
  fname = modelFolderName + "/" + clfname + aspect
  joblib.dump(clf, fname)
  print("Save model to %s" % fname)
  return 0

def loadModel(aspect):
  mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
  fname = mdir + modelFolderName + "/" + clfname + aspect
  clf = joblib.load(fname)
  return clf

def kFoldCrossValidation(clf,data,labels,k):
  data=np.array(data)
  labels=np.array(labels)
  scores = cross_validation.cross_val_score(clf, data, labels, cv=k, scoring='precision')
  print("precision: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  scores = cross_validation.cross_val_score(clf, data, labels, cv=k, scoring='recall')
  print("recall: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  scores = cross_validation.cross_val_score(clf, data, labels, cv=k, scoring='accuracy')
  print("accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  scores = cross_validation.cross_val_score(clf, data, labels, cv=k, scoring='f1')
  print("f1: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  print ""

def train():
  mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
  fpath = mdir + polarityfile
  line1 = open(fpath,'r').read().split("\n")[0]
  predDict=json.loads(line1)

  fpath = mdir + dictfile
  line1 = open(fpath,'r').read().split("\n")[0]
  wordsDict=json.loads(line1)

  data=[]
  datas={}
  labels={}
  aslen=len(aspectarr)
  
  #obtain data and labels
  wnl = WordNetLemmatizer()
  tokenizer = RegexpTokenizer(r'\w+') # Only keeps chars in the set [a-zA-Z0-9_] and removes punctuations
  rlines=open(datafile,'r').read().split('\n')
  lines=[x for x in rlines if len(x)>0] # filter empty strings
  snum=len(lines)
  wordCount=len(wordsDict.keys())

  for i in range(aslen):
    labels[aspectarr[i]]=[]
    datas[aspectarr[i]]=[]

  for i in range(snum):
    try:
      infodict=json.loads(lines[i])
    except:
      print "err:",lines[i]
      continue
    sent=infodict["sentence"]
    rwords=tokenizer.tokenize(sent)
    words=[wnl.lemmatize(x).lower() for x in rwords]
    feature=[0]*wordCount
    allwords=wordsDict.keys()
    for word in words:
      if word in allwords:
        j=wordsDict[word]["index"]
        feature[j]=wordsDict[word]["count"]
    data.append(feature)
    for j in range(aslen):
      val=infodict[aspectarr[j]]
      if val!=0:
        if val==1:
          labels[aspectarr[j]].append(1)
        elif val==2:
          labels[aspectarr[j]].append(-1)
        elif val==3:
          labels[aspectarr[j]].append(0)
        datas[aspectarr[j]].append(feature)

  for i in range(aslen):
    #clf = linear_model.LogisticRegression()
    clf=svm.LinearSVC(C=1.0, loss='l2',random_state=1,class_weight='auto')
    clf.fit(datas[aspectarr[i]], labels[aspectarr[i]])
    saveModel(clf,aspectarr[i])
    kFoldCrossValidation(clf,datas[aspectarr[i]], labels[aspectarr[i]],10)



def get_feature(sent):
  mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
  dictf=mdir +dictfile
  line1=open(dictf,'r').read().split("\n")[0]
  wordsDict=json.loads(line1)
  allwords=wordsDict.keys()
  wordCount=len(allwords)
  
  tokenizer = RegexpTokenizer(r'\w+')
  wnl = WordNetLemmatizer()
  
  rwords=tokenizer.tokenize(sent)
  words=[wnl.lemmatize(x).lower() for x in rwords]
  feature=[0]*wordCount
  
  for word in words:
    if word in allwords:
      j=wordsDict[word]["index"]
      feature[j]=wordsDict[word]["count"]
  return feature


# get the polarity prediction
def predict(rtext):
  feature=get_feature(rtext)
  aslen=len(aspectarr)
  parr = logistic_regression.predict(rtext).split(" ")
  ret = ""
  for i in range(aslen):
    if int(parr[i])!=0:
      clf=loadModel(aspectarr[i])
      ret+=str(clf.predict(feature)[0])+" "
    else:
      ret+="0 "
  return ret


def main():
  train()



if __name__ == "__main__":
  main()