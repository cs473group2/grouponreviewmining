import json
import sys

# Get dictionary of words from infile to outfile.
def printStat(infile):
  # Get lines of json objects
  rlines=open(infile,'r').read().split('\n') # split whole file into a list of lines
  lines=[line for line in rlines if len(line)>0] # remove empty lines
  linenum=len(lines)
  aspects=["_ambience", "_food", "_price", "_service", "_groupon", "_location" ]
  counts=[0,0,0,0,0,0]
  totalCount=0
  for i in range(linenum):
    try:
      infodict=json.loads(lines[i])
      totalCount+=1
      for j in range(6):
        if infodict[aspects[j]]>0:
          counts[j]+=1
    except:
      print i,lines[i]
  print "%s Stat:"%(infile)
  for j in range(6):
    if (totalCount!=0):
      fraction=counts[j]*100.0/totalCount
    else:
      fraction=0.0
    print "  %s counts: %d/%d = %.2f%%" %(aspects[j],counts[j],totalCount,fraction)

def main():
  if len(sys.argv)!=2:
    print "Usage: python printStat.py file.json"
    return
  datafile=sys.argv[1]
  printStat(datafile)

if __name__ == "__main__":
  main()