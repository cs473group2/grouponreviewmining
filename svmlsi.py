import json
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize
from sklearn import svm
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.externals import joblib
from nltk.metrics.scores import precision, recall
import sys
import os

folderName = "svmSavedModel/"
clfName = folderName + "svmclf.pkl"
lsaName = folderName + "svmlsa.pkl"
CVName = folderName + "svmCV.pkl"

tagName = ["_ambience", "_food", "_groupon", "_location", "_price", "_service"]

def saveModel(clf, lsa, CV, clfName, lsaName, CVName):
    joblib.dump(clf, clfName)
    joblib.dump(lsa, lsaName)
    joblib.dump(CV, CVName)
    
def loadModel(clfName, lsaName, CVName):
    clf = joblib.load(clfName)
    lsa = joblib.load(lsaName)
    CV = joblib.load(CVName)
    return (clf, lsa, CV)

class LemmaTokenizer(object):
    def __init__(self):
        self.wnl = WordNetLemmatizer()
    def __call__(self, doc):
        return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]
    
def my_tokenizer(s):
    return s.split()

def readFile(fileName, targetTag, stopwordlistFile):
    print("Reading file from " + fileName)
    rlines = open(fileName, 'r').read().split('\n')  # split whole file into a list of lines
    lines = [line for line in rlines if len(line) > 0]  # remove empty lines
    # Get stopword list
    stopwordList = open(stopwordlistFile, "r").read().split()
    textBag = []
    tag = []
    
    for line in lines:
        # print(line)
        jobj = json.loads(line)
        sentence = jobj["sentence"]
        ttag = jobj[targetTag]
        textBag.append(str(sentence))
        if(ttag == 0):
            tag.append(0)
        else:
            tag.append(1)
    print(len(textBag))

#     CV = CountVectorizer(tokenizer=LemmaTokenizer(), stop_words='english')
    CV = CountVectorizer(tokenizer=LemmaTokenizer(), stop_words=stopwordList)

    x = CV.fit_transform(textBag)
    print(x.shape[0], x.shape[1])

    return (x, CV, textBag, tag)

def LSA_data(x, CV, nFeatures):
    print("process data using LSA")
    lsa = TruncatedSVD(n_components=nFeatures)
    print("Shape before LSA (%d, %d)" % (x.shape[0], x.shape[1]))
    data = lsa.fit_transform(x)
    print("Shape after LSA (%d, %d)" % (data.shape[0], data.shape[1]))
    return (data, lsa)

def train(data, Tag):
    print("Training...")
    print(data.shape[0], data.shape[1])

    clf = svm.SVC()
    clf.fit(data[:],Tag[:])
    
    return clf

def test(clf, testData):
    #print("Testing...")
    #print("Testdata shape is (%d, %d)" % (testData.shape[0], testData.shape[1]))
    preditResult = clf.predict(testData[:])
    return preditResult

def test_withTag(clf, testData, testTag):
    print("Testing with Tag Known...")
    print(len(testData))
    print(len(testTag))
    preditResult = clf.predict(testData[:])
    print(len(preditResult))
    testTag = testTag[:]
    a = 0
    b = 0
    c = 0
    d = 0
    for i in range(len(preditResult)):
#         print(i)
        if preditResult[i] == 1 and testTag[i] == 1:
            a += 1
        elif preditResult[i] == 1 and testTag[i] == 0:
            b += 1
        elif preditResult[i] == 0 and testTag[i] == 1:
            c += 1
        else:
            d += 1
    
    print(a, b, c, d)
    if (a + b) == 0 or (a + c) == 0 or a == 0:
        precision = 0.0
        recall = 0.0
        f1 = 0.0
    else:
        precision = a * 1.0/(a + b)
        recall = a * 1.0/(a + c)
        f1 = 2.0 * precision * recall / (precision + recall)

    return (precision, recall, f1)

def main(tagName):
    x, CV, textBag, tag = readFile("reviewed.json", tagName, "stopwordList.txt")
    
    bestn = 1
    bestp = 0.0
    bestc = 0.0
    bestf1 = 0.0
    
    numOfTrain = len(textBag) * 8 / 10
    print("numberOfTrain is %d" % numOfTrain)

    trainTag = tag[:numOfTrain]
    testTag = tag[numOfTrain:]
    for nFeatures in range(1, 50):
        trainData, lsa = LSA_data(x, CV, nFeatures)
        testData = trainData[numOfTrain:]
        trainData = trainData[:numOfTrain]
        clf = train(trainData, trainTag)
        
        p, c, f1 = test_withTag(clf, testData, testTag)
        if f1 > bestf1:
            bestn = nFeatures
            bestp = p
            bestc = c
            bestf1 = f1
        print(nFeatures, p, c, f1)
    
    print("Best parameter for %s: " % tagName)
    print("Concept space dimension: " + str(bestn))
    print("precision: " + str(bestp))
    print("recall: " + str(bestc))
    print("f1: " + str(bestf1))
    
    print("Saving clf and lsa...")
    numOfTrain = len(textBag)

    trainTag = tag[:]
    nFeatures = bestn
    trainData, lsa = LSA_data(x, CV, nFeatures)
    clf = train(trainData, trainTag)
    saveModel(clf, lsa, CV, clfName + tagName, lsaName + tagName, CVName + tagName)
    print("Save done.")
    
    
def main_2():
    rawtext = ["I love the steak!"]
    print("len of rawtest is %d" % len(rawtext))
    x, CV, textBag, foodTag = readFile("labld_reviews-1-200.json", "_food", "stopwordList.txt")
    
    xx = CV.transform(rawtext)
    print(xx.shape[0], xx.shape[1])
        
    numOfTrain = len(textBag) / 2

    trainTag = foodTag[:numOfTrain]
    testTag = [1]
    numOfRight = 0
    for nFeatures in range(22, 50):
        trainData, lsa = LSA_data(x, CV, nFeatures)
        testData = lsa.transform(xx)
        trainData = trainData[:numOfTrain]
        clf = train(trainData, trainTag)
        
        predict = test(clf, testData)
        print(predict)
        if(predict[0] == testTag[0]):
            numOfRight += 1
    print("right # is %d" % numOfRight)
    
def main_3(rawtext, tagName):
    rawtext = [rawtext]
#    print(clfName + tagName)
    mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
    clf, lsa, CV = loadModel(mdir + clfName + tagName, mdir + lsaName + tagName, mdir + CVName + tagName)
    xx = CV.transform(rawtext)
    #print(xx.shape[0], xx.shape[1])
    testData = lsa.transform(xx)
    predict = test(clf, testData)
    return str(predict[0])
    
def rawTest(rawtext):
    prediction = ""
    for i in range(len(tagName)):
        prediction += main_3(rawtext, tagName[i])
        prediction += " "
#    print(prediction)
    return prediction

if __name__ == "__main__":
    if len(sys.argv) > 1:
        rawtext = sys.argv[1]
        result = rawTest(rawtext)
        print(result)
    else:
        for i in range(len(tagName)):
            main(tagName[i])
