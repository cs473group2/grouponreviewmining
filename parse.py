import json
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from sklearn import linear_model,cross_validation,metrics,svm,naive_bayes
import numpy as np
from sklearn.externals import joblib

clfName = "Logistic Regression"
modelFolderName = "LGSavedModel"
aspectarr = ["_ambience", "_food", "_groupon", "_location", "_price", "_service"]

def saveModel(clf, tag):
    fname = modelFolderName + "/" + clfName + tag
    joblib.dump(clf, fn)
    print("Save model to %s" % fname)
    return 0

def loadModel(tag):
    mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
    fname = mdir + modelFolderName + "/" + clfName + tag;
    clf = joblib.load(fname)
    return clf

# Get dictionary of words from infile to outfile.
def preprocess(infile,outfile,stopwordFile,cutsize):
  # Get lines of json objects
  rlines=open(infile,'r').read().split('\n') # split whole file into a list of lines
  lines=[line for line in rlines if len(line)>0] # remove empty lines
  linenum=len(lines)
  # Get stopword list
  stopwordList=open(stopwordFile,'r').read().split()
  wordsDict={} # words dictionary
  wnl = WordNetLemmatizer()
  tokenizer = RegexpTokenizer(r'\w+') # Only keeps chars in the set [a-zA-Z0-9_] and removes punctuations
  for i in range(linenum):
    try:
      infodict=json.loads(lines[i])
      sent=infodict["sentence"]
      rwords=tokenizer.tokenize(sent)
      words=[wnl.lemmatize(x).lower() for x in rwords]
      for word in words:
        if word not in stopwordList and len(word)>1:
          if word in wordsDict:
            wordsDict[word]["count"]+=1
          else:
            wordsDict[word]={"count":1}
    except:
      print i,lines[i]
  rwordsList=sorted(wordsDict.keys())
  index=0
  finalWordDict={}
  for word in rwordsList:
    if wordsDict[word]["count"]>cutsize:
      finalWordDict[word]={}
      finalWordDict[word]["count"]=wordsDict[word]["count"]
      finalWordDict[word]["index"]=index
      index+=1
  outf=open(outfile,"w")
  outf.write(json.dumps(finalWordDict,sort_keys=True)+"\n")
  outf.close()

# k fold cross validation
def kFoldCrossValidation(clf,data,labels,k,prompt):
  print prompt
  data=np.array(data)
  labels=np.array(labels)
  scores = cross_validation.cross_val_score(clf, data, labels, cv=k, scoring='precision')
  print("precision: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  scores = cross_validation.cross_val_score(clf, data, labels, cv=k, scoring='recall')
  print("recall: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  scores = cross_validation.cross_val_score(clf, data, labels, cv=k, scoring='accuracy')
  print("accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  scores = cross_validation.cross_val_score(clf, data, labels, cv=k, scoring='f1')
  print("f1: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  print ""

# get precision and recall
def evaluate(labels,predictions):
  totalOnes=0
  trueOnes=0
  falseOnes=0
  arrlen=len(labels)
  for i in range(arrlen):
    if labels[i]==1:
      totalOnes+=1
    if predictions[i]==1:
      if labels[i]==1:
        trueOnes+=1
      else:
        falseOnes+=1

  precision=(trueOnes*1.0)/(trueOnes+falseOnes)
  if(totalOnes==0):
    recall=1.0
  else:
    recall=trueOnes*1.0/totalOnes

  print trueOnes, falseOnes, totalOnes
  print "precision: ",precision
  print "recall: ",recall

def parse(datafile,dictfile):
  line1=open(dictfile,'r').read().split("\n")[0]
  wordsDict=json.loads(line1)
  data=[]
  labels={}
  labels["_ambience"]=[]
  labels["_food"]=[]
  labels["_groupon"]=[]
  labels["_location"]=[]
  labels["_price"]=[]
  labels["_service"]=[]
  #obtain data and labels
  wnl = WordNetLemmatizer()
  tokenizer = RegexpTokenizer(r'\w+') # Only keeps chars in the set [a-zA-Z0-9_] and removes punctuations
  rlines=open(datafile,'r').read().split('\n')
  lines=[x for x in rlines if len(x)>0] # filter empty strings
  snum=len(lines)
  wordCount=len(wordsDict.keys())
  for i in range(snum):
    try:
      infodict=json.loads(lines[i])
    except:
      print "err:",lines[i]
      continue
    sent=infodict["sentence"]
    rwords=tokenizer.tokenize(sent)
    words=[wnl.lemmatize(x).lower() for x in rwords]
    feature=[0]*wordCount
    allwords=wordsDict.keys()
    for word in words:
      if word in allwords:
        j=wordsDict[word]["index"]
        feature[j]=wordsDict[word]["count"]
    data.append(feature)
    if infodict["_ambience"]!=0:
      labels["_ambience"].append(1)
    else:
      labels["_ambience"].append(0)

    if infodict["_food"]!=0:
      labels["_food"].append(1)
    else:
      labels["_food"].append(0)

    if infodict["_groupon"]!=0:
      labels["_groupon"].append(1)
    else:
      labels["_groupon"].append(0)

    if infodict["_location"]!=0:
      labels["_location"].append(1)
    else:
      labels["_location"].append(0)

    if infodict["_price"]!=0:
      labels["_price"].append(1)
    else:
      labels["_price"].append(0)

    if infodict["_service"]!=0:
      labels["_service"].append(1)
    else:
      labels["_service"].append(0)
  
  #logistic regression!!
  #clf = linear_model.LogisticRegression() #penalty='l2', dual=False, tol=0.01, C=1.0, fit_intercept=True, intercept_scaling=1, class_weight=None, random_state=None)
  
  clf = svm.SVC()

  #clf = naive_bayes.GaussianNB()

  #cmt1="""
  k=10
  #print datafile,"stats:"
  kFoldCrossValidation(clf,data,labels["_ambience"],k,"Ambience:")
  kFoldCrossValidation(clf,data,labels["_food"],k,"Food:")
  kFoldCrossValidation(clf,data,labels["_groupon"],k,"Groupon:")
  kFoldCrossValidation(clf,data,labels["_location"],k,"Location:")
  kFoldCrossValidation(clf,data,labels["_price"],k,"Price:")
  kFoldCrossValidation(clf,data,labels["_service"],k,"Service:")
  print ""
  #"""


  cmt2="""
  datanum=len(data)
  trainnum=datanum*9/10
  print datanum,trainnum,"\n"

  clf.fit(data[:trainnum],labels["_food"][:trainnum])
  prdicta = clf.predict(data[trainnum:])
  prdictb = np.array(labels["_food"][trainnum:])
  print "eval food:"
  evaluate(prdictb,prdicta)

  clf.fit(data[:trainnum],labels["_service"][:trainnum])
  prdicta = clf.predict(data[trainnum:])
  prdictb = np.array(labels["_service"][trainnum:])
  print "\neval service:"
  evaluate(prdictb,prdicta)

  clf.fit(data[:trainnum],labels["_ambience"][:trainnum])
  prdicta = clf.predict(data[trainnum:])
  prdictb = np.array(labels["_ambience"][trainnum:])
  print "\neval ambience:"
  evaluate(prdictb,prdicta)

  clf.fit(data[:trainnum],labels["_location"][:trainnum])
  prdicta = clf.predict(data[trainnum:])
  prdictb = np.array(labels["_location"][trainnum:])
  print "\neval location:"
  evaluate(prdictb,prdicta)

  clf.fit(data[:trainnum],labels["_groupon"][:trainnum])
  prdicta = clf.predict(data[trainnum:])
  prdictb = np.array(labels["_groupon"][trainnum:])
  print "\neval groupon:"
  evaluate(prdictb,prdicta)

  clf.fit(data[:trainnum],labels["_price"][:trainnum])
  prdicta = clf.predict(data[trainnum:])
  prdictb = np.array(labels["_price"][trainnum:])
  print "\neval price:"
  evaluate(prdictb,prdicta)
  """




def main():
  cmt1="""datafile='reviewed.json'
  dictfile='small-words-dict.txt'
  preprocess(datafile,dictfile,'stopwordList.txt')
  parse(datafile,dictfile)
  """

  #cmt2="""
  dictfile='small-words-dict.txt'
  #datafiles=["reviewed-1-200.json","reviewed-201-400.json","reviewed-401-600.json","reviewed-601-800.json","reviewed-801-1000.json","reviewed.json"]
  #datafiles=["reviewed-1-200.json","reviewed-201-400.json","reviewed-401-600.json","reviewed-801-1000.json","reviewed.json"]
  datafiles=["allreviewed.json"]
  filenum=len(datafiles)
  for i in range(filenum):
    for j in range(1):
      print "\ncutsize j:",j
      preprocess(datafiles[i],dictfile,'stopwordList.txt',j)
      parse(datafiles[i],dictfile)
  #"""


if __name__ == "__main__":
  main()