import json
import nltk
import re
from nltk.corpus import stopwords
from nltk import stem
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn.decomposition import TruncatedSVD
import numpy

from sklearn.externals import joblib
import sys
import os

json_file = "reviewed.json"
cop_file = "cop.txt"
vecdata_file = "vecData.txt"
modelFolderName = "simpleSVMsavedModel"
clfName = "simpleSVMclf.pkl"


TAGNAME = ["_ambience", "_food", "_groupon", "_location", "_price", "_service"]


def saveModel(clf, tag):
    fn = modelFolderName + "/" + clfName + tag
    joblib.dump(clf, fn)
    print("Save model to %s" % fn)
    return 0

def loadModel(tag):
    mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
    fn = mdir + modelFolderName + "/" + clfName + tag;
    clf = joblib.load(fn)
    return clf

def loadCop(filename):
#    print("debug: %s" % str(os.path.dirname(os.path.realpath(__file__))))
    filename = str(os.path.dirname(os.path.realpath(__file__))) + "/" + filename
    rlines = open(filename, 'r').read().split('\n')
    ret = [line for line in rlines if len(line)>0]
    return ret

def preprocess(sent):
    ret = []
    sent = sent.lower()
    tokenized_list = nltk.word_tokenize(sent)
    stemmer = stem.snowball.EnglishStemmer()

    for word in tokenized_list:
        if re.match("[a-z]*$",word):
            if(word[-1]=='.' or word[-1]==','):
                word = word[:-1]
            word = stemmer.stem(word)
            if word not in stopwords.words('english'):
                ret.append(word)
    
    return ret


def readSent(fileName):
    inf = open(fileName, 'r')
    retSent = []
    retTag = {}
    for tags in TAGNAME:
        retTag[tags] = []
                 
    for line in inf:
        tmp = json.loads(line)
        retSent.append(tmp["sentence"])
        for tags in TAGNAME:
            if tmp[tags] != 0:
                retTag[tags].append(1)
            else:
                retTag[tags].append(0)
    inf.close()
    return (retSent, retTag)
                   

def sent2vec(sent, cop):
#    print("len(cop) is %d" % len(cop))
    ret = [0] * len(cop)
#    print("len(ret) is %d" % len(ret))
    sent = preprocess(sent)
    for word in sent:
        if word in cop:
#            print(cop.index(word))
            ret[cop.index(word)] += 1
            
    return ret


def json2vec(cop):
    sents, tags = readSent(json_file)
    outf = open(vecdata_file, 'w')
    outf.write(str(len(cop)) + "\n")

    for i in range(len(sents)):
        tmp = {}
        for tag in TAGNAME:
            tmp[tag] = tags[tag][i]
        sentv = sent2vec(sents[i], cop)
        for i in range(len(sentv)):
            if sentv[i] != 0:
                tmp[i] = sentv[i]
        json.dump(tmp, outf, sort_keys=True)
        outf.write("\n")
    outf.close()


def loadJsonVec():
    inf = open(vecdata_file, 'r')
    demension = int(inf.readline())
    start = 0
    retVec = []
    retTag = []
    for line in inf:
        if start == 0:
            start = 1
        else:
            jobj = json.loads(line)
            tmp = [0] * demension
            tmptag = {}
            for tag in TAGNAME:
                tmptag[tag] = jobj[tag]
            retTag.append(tmptag)

            for key,value in jobj.iteritems():
               if key[0] != "_":
                   tmp[int(key)] = int(value)
            retVec.append(tmp)
    inf.close()
    return (retVec, retTag)

def train(trainData, trainTag, num):
    clf = LinearSVC(C=1.0, loss='l2',random_state=1,class_weight='auto')
#    clf = SVC(kernel='linear', class_weight='auto')
    clf.fit(numpy.array(trainData[:num]), numpy.array(trainTag[:num]))
    return clf

def test(testData, clf):
    # ATTENTION! [testData]
    ret = clf.predict(testData)
    return ret
    
def cross(tagname):
    
    data, ttag  = loadJsonVec()
    print("cross val tag name is %s" % tagname)
    print("data set size is %d" % len(data))

    tag = [ a[tagname] for a in ttag]

    totalNum = len(data)
    trainNum = totalNum * 8 / 10
    trainData = data[:trainNum]
    trainTag = tag[:trainNum]
    testData = data[trainNum:]
    testTag = tag[trainNum:]
    
    clf = train(trainData, trainTag, trainNum);
    preditResult = test(testData, clf)
    
    a = 0
    b = 0
    c = 0
    d = 0
    for i in range(len(preditResult)):
#         print(i)
        if preditResult[i] == 1 and testTag[i] == 1:
            a += 1
        elif preditResult[i] == 1 and testTag[i] == 0:
            b += 1
        elif preditResult[i] == 0 and testTag[i] == 1:
            c += 1
        else:
            d += 1
    
    print(a, b, c, d)
    if (a + b) == 0 or (a + c) == 0 or a == 0:
        precision = 0.0
        recall = 0.0
        f1 = 0.0
    else:
        precision = a * 1.0/(a + b)
        recall = a * 1.0/(a + c)
        f1 = 2.0 * precision * recall / (precision + recall)

    print (precision, recall, f1)

    """
    clf = train(data, tag, len(data))
    saveModel(clf, tagname)
    """
    return (precision, recall, f1)

def lsa(nFeatures, tagname):
    data, ttag  = loadJsonVec()
    print("cross val tag name is %s" % tagname)
    print("data set size is %d" % len(data))

    tag = [ a[tagname] for a in ttag]

    totalNum = len(data)
    trainNum = totalNum * 8 / 10
    
    print("process data using LSA")
    lsa = TruncatedSVD(n_components=nFeatures)
    print("Shape before LSA (%d, %d)" % (len(data), len(data[0])))
    data = lsa.fit_transform(numpy.array(data))
    print("Shape after LSA (%d, %d)" % (len(data), len(data[0])))

    trainData = data[:trainNum]
    trainTag = tag[:trainNum]
    testData = data[trainNum:]
    testTag = tag[trainNum:]
    

    clf = train(trainData, trainTag, trainNum);
    preditResult = test(testData, clf)
    
    a = 0
    b = 0
    c = 0
    d = 0
    for i in range(len(preditResult)):
#         print(i)
        if preditResult[i] == 1 and testTag[i] == 1:
            a += 1
        elif preditResult[i] == 1 and testTag[i] == 0:
            b += 1
        elif preditResult[i] == 0 and testTag[i] == 1:
            c += 1
        else:
            d += 1
    
    print(a, b, c, d)
    if (a + b) == 0 or (a + c) == 0 or a == 0:
        precision = 0.0
        recall = 0.0
        f1 = 0.0
    else:
        precision = a * 1.0/(a + b)
        recall = a * 1.0/(a + c)
        f1 = 2.0 * precision * recall / (precision + recall)

    print (precision, recall, f1)

    """
    clf = train(data, tag, len(data))
    saveModel(clf, tagname)
    """
    return (precision, recall, f1)


def rawTest(rawtext):
    ret = ""
    cop = loadCop(cop_file)
    senv = sent2vec(rawtext, cop)
    for tag in TAGNAME:
        clf = loadModel(tag)
        # !!!
        pred = test(senv, clf)
        ret += str(pred[0]) + " "
    return ret
    

if __name__ == "__main__":
    if len(sys.argv) > 1:
        rawtext = sys.argv[1]
        result = rawTest(rawtext)
        print(result)
    else:
       cop = loadCop(cop_file)
       for tag in TAGNAME:
           bestn = 0
           bestf1 = 0.0
           for nFeatures in range(2, 100):
               p, r, f1 = lsa(nFeatures, tag)
               if f1 > bestf1:
                   bestf1 = f1
                   bestn = nFeatures
           print("Best n  is %d" % bestn)
           print("Best f1 is %f" % bestf1)
#           cross(tag)
#       json2vec(cop)



