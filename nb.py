import json
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from sklearn.naive_bayes import GaussianNB
import numpy as np

# Get dictionary of words from infile to outfile.
def preprocess(infile,outfile,stopwordFile):
  # Get lines of json objects
  rlines=open(infile,'r').read().split('\n') # split whole file into a list of lines
  lines=[line for line in rlines if len(line)>0] # remove empty lines
  linenum=len(lines)
  # Get stopword list
  stopwordList=open(stopwordFile,'r').read().split()
  wordsDict={} # words dictionary
  wnl = WordNetLemmatizer()
  tokenizer = RegexpTokenizer(r'\w+') # Only keeps chars in the set [a-zA-Z0-9_] and removes punctuations
  for i in range(linenum):
    try:
      infodict=json.loads(lines[i])
      sent=infodict["sentence"]
      rwords=tokenizer.tokenize(sent)
      words=[wnl.lemmatize(x).lower() for x in rwords]
      for word in words:
        if word not in stopwordList and len(word)>1:
          if word in wordsDict:
            wordsDict[word]["count"]+=1
          else:
            wordsDict[word]={"count":1}
    except:
      print i,lines[i]
  rwordsList=sorted(wordsDict.keys())
  index=0
  for word in rwordsList:
    wordsDict[word]["index"]=index
    index+=1
  outf=open(outfile,"w")
  outf.write(json.dumps(wordsDict,sort_keys=True)+"\n")
  outf.close()

# get precision and recall
def evaluate(labels,predictions):
  totalOnes=0
  trueOnes=0
  falseOnes=0
  arrlen=len(labels)
  for i in range(arrlen):
    if labels[i]==1:
      totalOnes+=1
    if predictions[i]==1:
      if labels[i]==1:
        trueOnes+=1
      else:
        falseOnes+=1
  if (trueOnes + falseOnes) == 0 or totalOnes == 0:
    precision = 0.0;
    recall = 0.0;
  else:
    precision=(trueOnes*1.0)/(trueOnes+falseOnes)
    recall=trueOnes*1.0/totalOnes
  print trueOnes, falseOnes, totalOnes
  print "precision: ",precision
  print "recall: ",recall

def parse(datafile,dictfile):
  line1=open(dictfile,'r').read().split("\n")[0]
  wordsDict=json.loads(line1)
  data=[]
  labels={}
  labels["_ambience"]=[]
  labels["_food"]=[]
  labels["_groupon"]=[]
  labels["_location"]=[]
  labels["_price"]=[]
  labels["_service"]=[]
  #obtain data and labels
  wnl = WordNetLemmatizer()
  tokenizer = RegexpTokenizer(r'\w+') # Only keeps chars in the set [a-zA-Z0-9_] and removes punctuations
  rlines=open(datafile,'r').read().split('\n')
  lines=[x for x in rlines if len(x)>0] # filter empty strings
  snum=len(lines)
  wordCount=len(wordsDict.keys())
  for i in range(snum):
    try:
      infodict=json.loads(lines[i])
    except:
      print "err:",lines[i]
      continue
    sent=infodict["sentence"]
    rwords=tokenizer.tokenize(sent)
    words=[wnl.lemmatize(x).lower() for x in rwords]
    feature=[0]*wordCount
    allwords=wordsDict.keys()
    for word in words:
      if word in allwords:
        j=wordsDict[word]["index"]
        feature[j]=wordsDict[word]["count"]
    data.append(feature)
    if infodict["_ambience"]!=0:
      labels["_ambience"].append(1)
    else:
      labels["_ambience"].append(0)

    if infodict["_food"]!=0:
      labels["_food"].append(1)
    else:
      labels["_food"].append(0)

    if infodict["_groupon"]!=0:
      labels["_groupon"].append(1)
    else:
      labels["_groupon"].append(0)

    if infodict["_location"]!=0:
      labels["_location"].append(1)
    else:
      labels["_location"].append(0)

    if infodict["_price"]!=0:
      labels["_price"].append(1)
    else:
      labels["_price"].append(0)

    if infodict["_service"]!=0:
      labels["_service"].append(1)
    else:
      labels["_service"].append(0)
  # Naive Bayes
  gnb = GaussianNB()
  trainNum = len(data) * 8 / 10
  gnb.fit(data[:trainNum],labels["_food"][:trainNum])
  prdicta = gnb.predict(data[trainNum:])
  prdictb = np.array(labels["_food"][trainNum:])
  print "eval food:"
  evaluate(prdictb,prdicta)

  gnb.fit(data[:trainNum],labels["_service"][:trainNum])
  prdicta = gnb.predict(data[trainNum:])
  prdictb = np.array(labels["_service"][trainNum:])
  print "\neval service:"
  evaluate(prdictb,prdicta)

  gnb.fit(data[:trainNum],labels["_ambience"][:trainNum])
  prdicta = gnb.predict(data[trainNum:])
  prdictb = np.array(labels["_ambience"][trainNum:])
  print "\neval ambience:"
  evaluate(prdictb,prdicta)

  gnb.fit(data[:trainNum],labels["_location"][:trainNum])
  prdicta = gnb.predict(data[trainNum:])
  prdictb = np.array(labels["_location"][trainNum:])
  print "\neval location:"
  evaluate(prdictb,prdicta)

  gnb.fit(data[:trainNum],labels["_groupon"][:trainNum])
  prdicta = gnb.predict(data[trainNum:])
  prdictb = np.array(labels["_groupon"][trainNum:])
  print "\neval groupon:"
  evaluate(prdictb,prdicta)

  gnb.fit(data[:trainNum],labels["_price"][:trainNum])
  prdicta = gnb.predict(data[trainNum:])
  prdictb = np.array(labels["_price"][trainNum:])
  print "\neval price:"
  evaluate(prdictb,prdicta)

def main():
  datafile='reviewed.json'
  dictfile='small-words.txt'
  
  preprocess(datafile,dictfile,'stopwordList.txt')
  parse(datafile,dictfile)

if __name__ == "__main__":
  main()
