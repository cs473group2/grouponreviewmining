import nltk
import json
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer

def countcmp(item):
    return item[1]

# Reads 'xxx.json' and output all the words and their occurences to 'words.txt'.

def main():
  # Get lines of json objects

  rlines=open('labld_reviews-1-200.json','r').read().split('\n') # split whole file into a list of lines

  lines=[line for line in rlines if len(line)>0] # remove empty lines
  linenum=len(lines)

  # Get stopword list
  stopwordList=open('stopwordList.txt','r').read().split()

  wordsDict={} # words dictionary
  wnl = WordNetLemmatizer()
  tokenizer = RegexpTokenizer(r'\w+') # Only keeps chars in the set [a-zA-Z0-9_] and removes punctuations
  for i in range(linenum):
    try:
      infodict=json.loads(lines[i])
      sent=infodict["sentence"]
      rwords=tokenizer.tokenize(sent)
      words=[wnl.lemmatize(x).lower() for x in rwords]
      for word in words:
        if word not in stopwordList and len(word)>1:
          if word in wordsDict:
            wordsDict[word]+=1
          else:
            wordsDict[word]=1
    except Exception, e:
        print str(e)
#      print i,lines[i]

  rwordsList=[]
  for word in wordsDict.keys():
    item=[word,wordsDict[word]]
    rwordsList.append(item)

  outf=open('small-words.txt',"w")
  wordList=sorted(rwordsList,key=countcmp)
  for item in wordList:
    outf.write(json.dumps(item)+"\n")
  outf.close()

  cmmt="""outf=open('small-words-dict.txt',"w")
  outf.write(json.dumps(wordsDict,sort_keys=True)+"\n")
  outf.close()"""


if __name__ == "__main__":
  main()
