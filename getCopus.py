import json
import nltk
import re
from nltk.corpus import stopwords
from nltk import stem


json_file = "reviewed.json"
copfile = "cop.txt"


def preprocess(sent):
    ret = []
    sent = sent.lower()
    tokenized_list = nltk.word_tokenize(sent)
    stemmer = stem.snowball.EnglishStemmer()

    for word in tokenized_list:
        if re.match("[a-z]*$",word):
            if(word[-1]=='.' or word[-1]==','):
                word = word[:-1]
            word = stemmer.stem(word)
            if word not in stopwords.words('english'):
                ret.append(word)
    
    return ret


def saveCop(fileName):
    inf = open(fileName, 'r')
    outf = open(copfile, "w")
    cop = []
    for line in inf:
        tmp = json.loads(line)
        sent = tmp["sentence"]
        sent = preprocess(sent)
        for word in sent:
            if word not in cop:
                cop.append(word)
    for word in cop:
        outf.write(word + "\n")
        
    inf.close()
    outf.close()
                   


if __name__ == "__main__":
    saveCop(json_file)
