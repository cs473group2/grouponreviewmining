import json
import nltk
import re
from nltk.corpus import stopwords
from nltk import stem
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn.decomposition import TruncatedSVD
import numpy

from sklearn.externals import joblib
import sys
import os

json_file = "allreviewed.json"
cop_file = "cop.txt"
vecdata_file = "vecData_POLAR.txt"
modelFolderName = "polaritySVMsavedModel"
clfName = "polaritySVMclf.pkl"
entry_file = "/home/wwwroot/group2.magic282.me/l.json"

TAGNAME = ["_ambience", "_food", "_groupon", "_location", "_price", "_service"]


def saveModel(clf, tag):
    fn = modelFolderName + "/" + clfName + tag
    joblib.dump(clf, fn)
    print("Save model to %s" % fn)
    return 0

def loadModel(tag):
    mdir = str(os.path.dirname(os.path.realpath(__file__))) + "/"
    fn = mdir + modelFolderName + "/" + clfName + tag;
    clf = joblib.load(fn)
    return clf

def loadCop(filename):
    #    print("debug: %s" % str(os.path.dirname(os.path.realpath(__file__))))
    filename = str(os.path.dirname(os.path.realpath(__file__))) + "/" + filename
    rlines = open(filename, 'r').read().split('\n')
    ret = [line for line in rlines if len(line)>0]
    return ret

def preprocess(sent):
    ret = []
    sent = sent.lower()
    tokenized_list = nltk.word_tokenize(sent)
    stemmer = stem.snowball.EnglishStemmer()
    
    for word in tokenized_list:
        if re.match("[a-z]*$",word):
            if(word[-1]=='.' or word[-1]==','):
                word = word[:-1]
            word = stemmer.stem(word)
            if word not in stopwords.words('english'):
                ret.append(word)
    
    return ret


def readSent(fileName):
    inf = open(fileName, 'r')
    retSent = []
    retTag = {}
    for tags in TAGNAME:
        retTag[tags] = []
    
    for line in inf:
        tmp = json.loads(line)
        retSent.append(tmp["sentence"])
        for tags in TAGNAME:
            retTag[tags].append(tmp[tags])
    inf.close()
    return (retSent, retTag)


def sent2vec(sent, cop):
    #    print("len(cop) is %d" % len(cop))
    ret = [0] * len(cop)
    #    print("len(ret) is %d" % len(ret))
    sent = preprocess(sent)
    for word in sent:
        if word in cop:
            #            print(cop.index(word))
            ret[cop.index(word)] += 1
    
    return ret


def json2vec(cop):
    sents, tags = readSent(json_file)
    outf = open(vecdata_file, 'w')
    outf.write(str(len(cop)) + "\n")
    
    for i in range(len(sents)):
        tmp = {}
        for tag in TAGNAME:
            tmp[tag] = tags[tag][i]
            #print(tmp[tag])
        sentv = sent2vec(sents[i], cop)
        for i in range(len(sentv)):
            if sentv[i] != 0:
                tmp[i] = sentv[i]
                    #print(tmp[tag])
        json.dump(tmp, outf, sort_keys=True)
        outf.write("\n")
    outf.close()
    print("Done with vec_data")


def loadJsonVec():
    inf = open(vecdata_file, 'r')
    demension = int(inf.readline())
    start = 0
    retVec = []
    retTag = []
    for line in inf:
        if start == 0:
            start = 1
        else:
            jobj = json.loads(line)
            tmp = [0] * demension
            tmptag = {}
            for tag in TAGNAME:
                tmptag[tag] = jobj[tag]
            retTag.append(tmptag)
            
            for key,value in jobj.iteritems():
                if key[0] != "_":
                    tmp[int(key)] = int(value)
            retVec.append(tmp)
    inf.close()
    return (retVec, retTag)

def train(trainData, trainTag, num):
    clf = LinearSVC(C=1.0, loss='l2',random_state=1,class_weight='auto')
    #    clf = SVC(kernel='linear', class_weight='auto')
    clf.fit(numpy.array(trainData[:num]), numpy.array(trainTag[:num]))
    return clf

def test(testData, clf):
    ret = clf.predict(testData)
    return ret


def cross(tagname):
    
    data, ttag  = loadJsonVec()
    #print("cross val tag name is %s" % tagname)
    #print("data set size is %d" % len(data))
    
    tag = [ a[tagname] for a in ttag]
    
    totalNum = len(data)
    trainNum = totalNum * 8 / 10
    trainData = data[:trainNum]
    trainTag = tag[:trainNum]
    testData = data[trainNum:]
    testTag = tag[trainNum:]
    
    clf = train(trainData, trainTag, trainNum);
    preditResult = test(testData, clf)
    
    goodPredictions = 0
    badPredictions = 0
    
    for i in range(len(preditResult)):
        #         print(i)
        if preditResult[i] == testTag[i]:
            goodPredictions += 1
#            if (preditResult[i] != 0 and preditResult[i] != 1):
#                print("YAHOO!!!")
        else:
            badPredictions += 1
    
    accuracy = (goodPredictions) * 1.0 / (goodPredictions + badPredictions)
    print("Current accuracy: " + str(accuracy))

    clf = train(data, tag, len(data))
    saveModel(clf, tagname)

    return (accuracy)


def rawTest(rawtext, relatedness_vector):
    ret = ""
    cop = loadCop(cop_file)
    senv = sent2vec(rawtext, cop)
    iter = 0
    for tag in TAGNAME:
        if relatedness_vector[iter] == 1:
            clf = loadModel(tag)
            pred = test(senv, clf)
            ret += str(pred[0]) + " "
        else:
            ret += "0 "
        iter += 1
    return ret

def doStuff():
    entry = open(entry_file, 'r')
    for line in entry:
        tmp = json.loads(line)
        break
    rawtext = tmp["sentence"]
    relatedness_vector = [0, 0, 0, 0, 0, 0]
    
    if (int(tmp["lr_ambience"]) + int(tmp["svm_ambience"]) + int(tmp["nb_ambience"]) + int(tmp["wn_ambience"]) >= 3):
        relatedness_vector[0] = 1
    if (int(tmp["lr_food"]) + int(tmp["svm_food"]) + int(tmp["nb_food"]) + int(tmp["wn_food"]) >= 3):
        relatedness_vector[1] = 1
    if (int(tmp["lr_groupon"]) + int(tmp["svm_groupon"]) + int(tmp["nb_groupon"]) + int(tmp["wn_groupon"]) >= 3):
        relatedness_vector[2] = 1
    if (int(tmp["lr_location"]) + int(tmp["svm_location"]) + int(tmp["nb_location"]) + int(tmp["wn_location"]) >= 3):
        relatedness_vector[3] = 1
    if (int(tmp["lr_price"]) + int(tmp["svm_price"]) + int(tmp["nb_price"]) + int(tmp["wn_price"]) >= 3):
        relatedness_vector[4] = 1
    if (int(tmp["lr_service"]) + int(tmp["svm_service"]) + int(tmp["nb_service"]) + int(tmp["wn_service"]) >= 3):
        relatedness_vector[5] = 1
    entry.close()
    # print(relatedness_vector)
    result = rawTest(rawtext, relatedness_vector)
    return result

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        result = doStuff()
        print(result)
    elif (sys.argv[1] == 'train'):
        cop = loadCop(cop_file)
        json2vec(cop)
        for tag in TAGNAME:
            accuracy = cross(tag)

#            bestNfeatures = 0
#            bestAccuracy = 0
#            for nFeatures in range(2, 30):
#                accuracy = lsa(nFeatures, tag, False)
#                if bestAccuracy < accuracy:
#                    bestNfeatures = nFeatures
#                    bestAccuracy = accuracy

            # Saving the model with best results
#            lsa(bestNfeatures, tag, True)
            print("----")
            print("Aspect: " + str(tag))
#            print("Best # features: " + str(bestNfeatures))
            print("Accuracy: " + str(accuracy))
            print("----")

